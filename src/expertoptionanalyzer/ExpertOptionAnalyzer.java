/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expertoptionanalyzer;

import java.awt.AlphaComposite;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.LinkedList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author ralpuchev
 */
public class ExpertOptionAnalyzer extends Thread{
    
    private Robot robot = null;
    private Dimension screenSize = null;
    
    /*
    Colores server tano
    amarillo linea media: 8,199,247
    verde inferior: 18,199,123-8,219,132-
    rojo superior: 0,0,255
    azul stock actual: 206,142,24
    rojo boton: 107,44,189
    verde boton: 132,199,24
    limite de compra: 115,117,115
    ventana resultados: 49,32,24
    */
    
    //limites de la parte central
    org.opencv.core.Point punto_superior_izquierdo_parte_central = null;
    org.opencv.core.Point punto_superior_derecho_parte_central = null;
    org.opencv.core.Point punto_inferior_izquierdo_parte_central = null;
    org.opencv.core.Point punto_inferior_derecho_parte_central = null;
    Rect rectangulo_central = null;
    Rect rectangulo_botones = null;
    Rect rectangulo_ventana = null;
    Rect rectangulo_resultados = null;
    int numero_imagen = 0;
    int compra_realizada = 0;
    LinkedList<Mat> lista_imagenes = null;
    int numero_intentos_buscando_ventana_resultados = 0;
    int numero_compras = 0;
    int limite_numero_compras = 100;//para evaluar con las otras pruebas de otras divisas y métodos
    Boolean ejecutando = true;
    LinkedList<Integer> lista_resultados = null;
    int aciertos = 0;
    
    public LinkedList<Integer> getListaDeResultados(){
        if(lista_resultados == null) lista_resultados = new LinkedList<Integer>();
        return lista_resultados;
    }
    
    public Rect getRectanguloCentral(){
        //server may
        //if(rectangulo_central == null) rectangulo_central = new Rect(punto_superior_izquierdo_parte_central, punto_inferior_derecho_parte_central);
        if(rectangulo_central == null) rectangulo_central = new Rect(punto_superior_izquierdo_parte_central, punto_inferior_derecho_parte_central);
        return rectangulo_central;
    }
    
    public Rect getRectagunloBotones(){
        //server may
        //if(rectangulo_botones == null) rectangulo_botones = new Rect(new org.opencv.core.Point(600,850), new org.opencv.core.Point(900,910));
        if(rectangulo_botones == null) rectangulo_botones = new Rect(new org.opencv.core.Point(600,1335), new org.opencv.core.Point(900,1400));
        return rectangulo_botones;
    }
    
    public Rect getRectanguloVentanaDeResultados(){
        //if(rectangulo_ventana == null) rectangulo_ventana = new Rect(new org.opencv.core.Point(400,300), new org.opencv.core.Point(900,670));
        if(rectangulo_ventana == null) rectangulo_ventana = new Rect(new org.opencv.core.Point(420,550), new org.opencv.core.Point(900,970));
        return rectangulo_ventana;
    }
    
    public Rect getRectanguloResultado(){
        //if(rectangulo_resultados == null) rectangulo_resultados = new Rect(new org.opencv.core.Point(620,920), new org.opencv.core.Point(720,970));
        if(rectangulo_resultados == null) rectangulo_resultados = new Rect(new org.opencv.core.Point(620,920), new org.opencv.core.Point(720,970));
        return rectangulo_resultados;
    }
    
    public Robot getRobot(){
        if(robot == null){
            try{
                robot = new Robot();
            }catch(Exception e){
                System.out.println("Error creando robot para captura de pantalla");
            }
        }
        return robot;
    }
    
    public Dimension getTamanioPantalla(){
        if(screenSize == null) screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        return screenSize;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        
        
        ExpertOptionAnalyzer analizador = new ExpertOptionAnalyzer();
        //analizador.pruebaDeMedidas();
        
        analizador.start();
    }
    
    public void run(){
        while(true){
            analizaAppDeBolsaDos();
            if(!this.ejecutando) return;
            try{
                sleep(20);
            }catch(Exception e){
                System.out.println("No se pudo dormir el hilo");
            }
        }
    }
    
    public void pruebaDeMedidas(){
        BufferedImage imagen = capturarPantalla(null);
        Mat imagen_original = (Mat)(getBufferedImageToMat(imagen)).submat(new Rect(new org.opencv.core.Point(0,0), new org.opencv.core.Point((imagen.getWidth()/2)+100,imagen.getHeight())));
        //muestraImagenEnVentana(convertirMatrizAImagen(imagen_original), "Primera imagen");
        
        //server may
        /*if(rectangulo_central == null){
            punto_superior_izquierdo_parte_central = new org.opencv.core.Point(70,140);
            punto_superior_derecho_parte_central = new org.opencv.core.Point((imagen_original.width())-90,140);
            punto_inferior_izquierdo_parte_central = new org.opencv.core.Point(70,840);
            punto_inferior_derecho_parte_central = new org.opencv.core.Point((imagen_original.width())-90,840);
        }*/
        if(rectangulo_central == null){
            punto_superior_izquierdo_parte_central = new org.opencv.core.Point(60,140);
            punto_superior_derecho_parte_central = new org.opencv.core.Point((imagen_original.width())-120,140);
            punto_inferior_izquierdo_parte_central = new org.opencv.core.Point(60,1330);
            punto_inferior_derecho_parte_central = new org.opencv.core.Point((imagen_original.width())-120,1330);
        }
        
        //puntos recuperados de la imagen
        org.opencv.core.Point punto_stock_actual = getPuntoDeStockActual(imagen_original);
        org.opencv.core.Point punto_limite_compra = getPuntoDeLineaLimiteDeCompra(imagen_original);
        org.opencv.core.Point punto_linea_superior = getUltimoPuntoDeLineaSuperior(imagen_original);
        org.opencv.core.Point punto_linea_verde = getUltimoPuntoDeLineaInferior(imagen_original);
        org.opencv.core.Point punto_linea_media = getUltimoPuntoDeLineaMedia(imagen_original);
        
        Core.rectangle(imagen_original, punto_superior_izquierdo_parte_central, punto_inferior_derecho_parte_central, new Scalar(0,0,255));
        Core.rectangle(imagen_original, getRectagunloBotones().tl(), getRectagunloBotones().br(), new Scalar(0,0,255));
        Core.rectangle(imagen_original, getRectanguloVentanaDeResultados().tl(), getRectanguloVentanaDeResultados().br(), new Scalar(0,0,255));
        Core.rectangle(imagen_original, getRectanguloResultado().tl(), getRectanguloResultado().br(), new Scalar(0,0,255));
        Core.line(imagen_original, punto_stock_actual, punto_stock_actual, new Scalar(0,255,0), 4);
        Core.line(imagen_original, punto_linea_superior, punto_linea_superior, new Scalar(0,255,0), 4);
        Core.line(imagen_original, punto_linea_verde, punto_linea_verde, new Scalar(0,255,0), 4);
        Core.line(imagen_original, punto_linea_media, punto_linea_media, new Scalar(0,255,0), 4);
        Core.line(imagen_original, punto_limite_compra, punto_limite_compra, new Scalar(0,255,0), 4);
        
        Highgui.imwrite("imagenes/prueba_"+numero_imagen+".png", imagen_original);
    }
    
    //Análisis contra tendencia.
    /*
    Cuando el precio del stock está cerca de la línea superior o inferior del precio, entonces se realiza la desición de compra
    contra tendencia, es decir, a la inversa de la dirección actual del precio del stock
    */
    public void analizaAppDeBolsa(){
        //recuperamos la imagen de la pantalla
        BufferedImage imagen = capturarPantalla(null);
        Mat imagen_original = (Mat)(getBufferedImageToMat(imagen)).submat(new Rect(new org.opencv.core.Point(0,0), new org.opencv.core.Point((imagen.getWidth()/2)+100,imagen.getHeight())));
        //muestraImagenEnVentana(convertirMatrizAImagen(imagen_original), "Media Mat");
        
        if(compra_realizada == 0){
            //recuperamos los puntos de interes de la imagen de la pantalla
            //server may
            /*if(rectangulo_central == null){
                punto_superior_izquierdo_parte_central = new org.opencv.core.Point(70,140);
                punto_superior_derecho_parte_central = new org.opencv.core.Point((imagen_original.width())-90,140);
                punto_inferior_izquierdo_parte_central = new org.opencv.core.Point(70,840);
                punto_inferior_derecho_parte_central = new org.opencv.core.Point((imagen_original.width())-90,840);
            }*/
            
            if(rectangulo_central == null){
                punto_superior_izquierdo_parte_central = new org.opencv.core.Point(60,140);
                punto_superior_derecho_parte_central = new org.opencv.core.Point((imagen_original.width())-300,140);
                punto_inferior_izquierdo_parte_central = new org.opencv.core.Point(60,1330);
                punto_inferior_derecho_parte_central = new org.opencv.core.Point((imagen_original.width())-300,1330);
            }

            //ubicamos el último punto en donde aparece la linea azul y tomamos esa referencia para la posición actual del stock
            org.opencv.core.Point punto_stock_actual = getPuntoDeStockActual(imagen_original);
            //si alguno de los puntos es null, entonces no se continua con el analisis
            if(punto_stock_actual == null) return;

            //recuperamos el punto de limite de compra para determinar que tan cercanos estamos al límite de compra
            org.opencv.core.Point punto_limite_compra = getPuntoDeLineaLimiteDeCompra(imagen_original);
            if(punto_limite_compra == null) return;

            //recuperamos las distancias para realizar el razonamiento
            double distancia_stock_limite_compra = punto_limite_compra.x-punto_stock_actual.x;
            
            

            //hacemos la compra cuando falten solo 10px para alcanzar la linea de compra
            if(distancia_stock_limite_compra <= 40 && distancia_stock_limite_compra >= 10){
                
                Core.rectangle(imagen_original, punto_superior_izquierdo_parte_central, punto_inferior_derecho_parte_central, new Scalar(0,0,255));
                Core.rectangle(imagen_original, getRectagunloBotones().tl(), getRectagunloBotones().br(), new Scalar(0,0,255));
                
                if(lista_imagenes == null) lista_imagenes = new LinkedList<Mat>();
                
                //System.out.println("Imagen: "+numero_imagen+" **************************");
                //System.out.println("Distancia con respecto al limite de compra: "+distancia_stock_limite_compra);

                //ubicamos el último punto en donde aparece la línea roja (limite superior)#20,0,200, #18,0,209 #14,0,226
                org.opencv.core.Point punto_linea_superior = getUltimoPuntoDeLineaSuperior(imagen_original);
                if(punto_linea_superior == null) return;

                //ubicamos el último punto en donde aparece la línea verde (limite infererior)#27,171,97 #24,178,102
                org.opencv.core.Point punto_linea_verde = getUltimoPuntoDeLineaInferior(imagen_original);
                if(punto_linea_verde == null) return;

                //ubicamos el último punto en donde aparece la línea amarilla (media movil) #17,187,237
                org.opencv.core.Point punto_linea_media = getUltimoPuntoDeLineaMedia(imagen_original);
                if(punto_linea_media == null) return;
                
                double distancia_stock_limite_superior = this.getDistanciaEuclidianaDeDosPuntos(punto_stock_actual, punto_linea_superior);
                double distancia_stock_linea_media = this.getDistanciaEuclidianaDeDosPuntos(punto_stock_actual, punto_linea_media);
                double distancia_stock_linea_inferior = this.getDistanciaEuclidianaDeDosPuntos(punto_stock_actual, punto_linea_verde);
                //si la distancia que hay con la linea media es menor que la distancia que hay con respecto a las lineas superior e inferior,
                //entonces no se podrá determinar si sube o baja y se mantendrá sin cambio
                if((distancia_stock_linea_media < distancia_stock_limite_superior) && (distancia_stock_linea_media < distancia_stock_linea_inferior)){
                    //como el punto actual del stock está más cerca de la línea media que alguno de los extremos, entonces no hacemos nada
                    //System.out.println("No se puede decidir por alguna opción, se espera al siguiente frame");
                }else{
                    //ahora verificamos los extremos, en esta forma de "razonamiento" vamos a actuar solo cuando el valor del stock esté por encima
                    //de los extremos, cualquiera de los dos y en consecuencia, la opción seleccionada será contra-tendencia
                    if(distancia_stock_limite_superior < distancia_stock_linea_inferior){
                        //se calcula la distancia que hay entre la linea superior y la linea media
                        double distancia_limite_superior_linea_media = this.getDistanciaEuclidianaDeDosPuntos(punto_linea_superior, punto_linea_media);
                        //quiere decir que estamos más cerca de arriba y que el precio del stock a sobrepasado la línea superior, es decir, está más caro que el límite superior, en ese caso vamos contra-tendencia (bajar)
                        if((punto_stock_actual.y <= punto_linea_superior.y)||((distancia_limite_superior_linea_media/4)>=Math.abs(punto_stock_actual.y-punto_linea_superior.y))){
                            //en este caso y solo entonces, vamos a elegir bajar como la opción contra-tendencia
                            System.out.println("contra-tendencia: bajar");
                            presionarBotonBajar(imagen_original);
                            compra_realizada = 1;
                            numero_compras++;
                        }else{
                            //si estamos más cerca de arriba pero no hemos sobrepasado el valor límite de la acción, entonces seguimos la tendencia (subir)
                            /*System.out.println("sigue-tendencia: subir");
                            presionarBotonSubir(imagen_original);
                            compra_realizada = 1;
                            numero_compras++;*/
                        }
                    }else{
                        //estamos más cerca de abajo
                        double distancia_limite_inferior_linea_media = this.getDistanciaEuclidianaDeDosPuntos(punto_linea_verde, punto_linea_media);
                        //quiere decir que estamos más cerca de arriba y que el precio del stock a sobrepasado la línea superior, es decir, está más barato que el límite inferior, en ese caso vamos contra-tendencia (subir)
                        if((punto_stock_actual.y >= punto_linea_verde.y)||((distancia_limite_inferior_linea_media/4)>=Math.abs(punto_stock_actual.y-punto_linea_verde.y))){
                            //en este caso y solo entonces, vamos a elegir bajar como la opción contra-tendencia
                            System.out.println("contra-tendencia: subir");
                            presionarBotonSubir(imagen_original);
                            compra_realizada = 1;
                            numero_compras++;
                        }else{
                            //si estamos más cerca de abajo pero no hemos sobrepasado el valor inferior de la acción, entonces seguimos la tendencia (bajar)
                            /*System.out.println("sigue-tendencia: bajar");
                            presionarBotonBajar(imagen_original);
                            compra_realizada = 1;
                            numero_compras++;*/
                        }
                    }
                }
                Core.line(imagen_original, punto_stock_actual, punto_stock_actual, new Scalar(0,255,0), 4);
                Core.line(imagen_original, punto_linea_superior, punto_linea_superior, new Scalar(0,255,0), 4);
                Core.line(imagen_original, punto_linea_verde, punto_linea_verde, new Scalar(0,255,0), 4);
                Core.line(imagen_original, punto_linea_media, punto_linea_media, new Scalar(0,255,0), 4);
                Core.line(imagen_original, punto_limite_compra, punto_limite_compra, new Scalar(0,255,0), 4);

                //guardamos las imagenes en la carpeta para comprobar que los puntos correspondan
                //Highgui.imwrite("imagenes/imagen_"+numero_imagen+".png", imagen_original);
                lista_imagenes.add(imagen_original.clone());
                numero_imagen++;
            }
        }else{
            //como ya se realizó la compra, entonces buscamos la ventana de resultados. Una vez que tengamos la ventana de resultados,
            //la guardamos, presionamos cualquier lado fuera de la ventana y posicionamos el mouse fuera de la ventana para que no cause problema
            //se debería reiniciar el proceso de busqueda de otra oportunidad
            org.opencv.core.Point punto_ventana = getPuntoMasDerechaDeVentanaDeResultados(imagen_original);
            if(punto_ventana == null) return;
            numero_intentos_buscando_ventana_resultados++;
            //System.out.println("Intentos buscando la ventana de resultados: "+numero_intentos_buscando_ventana_resultados);
               
            //ahora que ya se encontró la ventana de resultados, pero hay que asegurarnos de que ya aparecieron los resultados de la transacción, es decir
            //que aparezca una cantidad en blanco, que sería o correspondería a pérdida y de color verde que correspondería a ganancia
            Boolean hay_resultado = true;
            //may 148,184,33
            if(hayColorDentroDeZonaDeResultados(imagen_original.submat(getRectanguloResultado()), new Scalar(127,149,32), new Scalar(148,182,33))){
                System.out.println("Resultado: 1");
                aciertos++;
            }else if(hayColorDentroDeZonaDeResultados(imagen_original.submat(getRectanguloResultado()), new Scalar(255,255,255), new Scalar(255,255,255))){
                System.out.println("Resultado: 0");
            }else hay_resultado = false;

            if(hay_resultado){
                //ya se mostró la ventana, entonces hacemos click fuera de ella para que desaparezca
                getRobot().mouseMove((int)200, (int)200);
                getRobot().mousePress(InputEvent.BUTTON1_DOWN_MASK);
                getRobot().mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                getRobot().mousePress(InputEvent.BUTTON1_DOWN_MASK);
                getRobot().mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                //después de que se quita la ventana de resultados, nos movemos fuera de la gráfica para no interferir con la visualización
                getRobot().mouseMove(0, 0);
                Highgui.imwrite("imagenes/resultado_"+numero_compras+".png", imagen_original);

                //escribimos las imagenes que tenemos en la lista de imagenes, ahorita que es posible
                int i=0;
                for(Mat image : lista_imagenes){
                    Highgui.imwrite("imagenes/imagen_"+numero_imagen+"_"+i+".png", image);
                    i++;
                }
                lista_imagenes = null;
                compra_realizada = 0;
                numero_intentos_buscando_ventana_resultados = 0;

                if(numero_compras >= this.limite_numero_compras){
                    System.out.println("Aciertos: "+aciertos+" Errores: "+(limite_numero_compras-aciertos)+" %Aciertos: "+(aciertos*100/limite_numero_compras)+" %Errores: "+(((limite_numero_compras-aciertos)*100)/limite_numero_compras));
                    this.ejecutando = false;
                    return;
                }
            }
        }
    }
    
    /*
    Esta versión del análisis toma las desiciones de compra de la siguiente forma:
    Si el precio del stock está cercano a la línea media, entonces no se realiza ninguna acción
    
    Este razonamiento es cuando se está muy cerca del límite de compra (40 - 10) pixeles, es decir, la ventana de observación es de 30px
    Si el precio del stock está cercano a algún límite, entonces se realiza el siguiente razonamiento:
        Si el precio del stock está dentro de 2x la distancia del límite opuesto con respecto de la línea media, entonces se sigue la tendencia -> "Apenas vamos hacia esa dirección, entonces seguimos la tendencia"
        Si el precio del stock > 2x la distancia del límite opuesto con respecto de la línea media, entonces se va contra tendencia -> "Ya estamos muy arriba de esa tendencia, entonces asumimos que habrá rebote"
    
    Cuando la ventana de visión está entre 200-41 Solo se responderá cuando el precio del stock vaya por arriba de 3x, donde x es la distancia que existe entre el límite contrario con respecto de la línea media y en
    contra tendencia
    */
    public void analizaAppDeBolsaDos(){
        //recuperamos la imagen de la pantalla
        BufferedImage imagen = capturarPantalla(null);
        Mat imagen_original = (Mat)(getBufferedImageToMat(imagen)).submat(new Rect(new org.opencv.core.Point(0,0), new org.opencv.core.Point((imagen.getWidth()/2)+100,imagen.getHeight())));
        //muestraImagenEnVentana(convertirMatrizAImagen(imagen_original), "Media Mat");
        
        if(compra_realizada == 0){
            //recuperamos los puntos de interes de la imagen de la pantalla
            //server may
            /*if(rectangulo_central == null){
                punto_superior_izquierdo_parte_central = new org.opencv.core.Point(70,140);
                punto_superior_derecho_parte_central = new org.opencv.core.Point((imagen_original.width())-90,140);
                punto_inferior_izquierdo_parte_central = new org.opencv.core.Point(70,840);
                punto_inferior_derecho_parte_central = new org.opencv.core.Point((imagen_original.width())-90,840);
            }*/
            
            if(rectangulo_central == null){
                punto_superior_izquierdo_parte_central = new org.opencv.core.Point(60,140);
                punto_superior_derecho_parte_central = new org.opencv.core.Point((imagen_original.width())-300,140);
                punto_inferior_izquierdo_parte_central = new org.opencv.core.Point(60,1330);
                punto_inferior_derecho_parte_central = new org.opencv.core.Point((imagen_original.width())-300,1330);
            }

            //ubicamos el último punto en donde aparece la linea azul y tomamos esa referencia para la posición actual del stock
            org.opencv.core.Point punto_stock_actual = getPuntoDeStockActual(imagen_original);
            //si alguno de los puntos es null, entonces no se continua con el analisis
            if(punto_stock_actual == null) return;

            //recuperamos el punto de limite de compra para determinar que tan cercanos estamos al límite de compra
            org.opencv.core.Point punto_limite_compra = getPuntoDeLineaLimiteDeCompra(imagen_original);
            if(punto_limite_compra == null) return;

            //recuperamos las distancias para realizar el razonamiento
            double distancia_stock_limite_compra = punto_limite_compra.x-punto_stock_actual.x;

            //ubicamos el último punto en donde aparece la línea roja (limite superior)#20,0,200, #18,0,209 #14,0,226
            org.opencv.core.Point punto_linea_superior = getUltimoPuntoDeLineaSuperior(imagen_original);
            if(punto_linea_superior == null) return;

            //ubicamos el último punto en donde aparece la línea verde (limite infererior)#27,171,97 #24,178,102
            org.opencv.core.Point punto_linea_verde = getUltimoPuntoDeLineaInferior(imagen_original);
            if(punto_linea_verde == null) return;

            //ubicamos el último punto en donde aparece la línea amarilla (media movil) #17,187,237
            org.opencv.core.Point punto_linea_media = getUltimoPuntoDeLineaMedia(imagen_original);
            if(punto_linea_media == null) return;

            /*double distancia_stock_limite_superior = this.getDistanciaEuclidianaDeDosPuntos(punto_stock_actual, punto_linea_superior);
            double distancia_stock_linea_media = this.getDistanciaEuclidianaDeDosPuntos(punto_stock_actual, punto_linea_media);
            double distancia_stock_linea_inferior = this.getDistanciaEuclidianaDeDosPuntos(punto_stock_actual, punto_linea_verde);*/
            
            double distancia_stock_limite_superior = Math.abs(punto_stock_actual.y-punto_linea_superior.y);
            double distancia_stock_linea_media = Math.abs(punto_stock_actual.y-punto_linea_media.y);
            double distancia_stock_linea_inferior = Math.abs(punto_stock_actual.y-punto_linea_verde.y);

            //hacemos la compra cuando falten solo 10px para alcanzar la linea de compra
            if(distancia_stock_limite_compra <= 50 && distancia_stock_limite_compra >= 10){
                //búsqueda de picos dentro del precio
                //Cuando la ventana de visión está entre 200-41 Solo se responderá cuando el precio del stock vaya por arriba de 3x, donde x es la distancia que existe entre el límite contrario con respecto de la línea media y en contra tendencia
                //esta acción debería ser "segura" ya que hay mayor probabilidad de que indique un piso o un techo en el precio del stock
                if((distancia_stock_linea_media > distancia_stock_limite_superior)&&(distancia_stock_limite_superior < distancia_stock_linea_inferior)){
                    //precio del stock pegado al límite superior
                    //si la distancia que hay entre la línea media y la línea inferior * 2 es menor a la distancia que hay entre el precio actual del stock y la línea media, entonces se trata de un pico al que debemos responder contra tendencia
                    if((Math.abs(punto_linea_verde.y-punto_linea_media.y)*1)<=Math.abs(punto_linea_media.y-punto_stock_actual.y)){
                        System.out.println("Pico encontrado contra-tendencia: bajar");
                        presionarBotonBajar(imagen_original);
                        compra_realizada = 1;
                        numero_compras++;
                        return;
                    }
                }else if((distancia_stock_linea_media > distancia_stock_linea_inferior)&&(distancia_stock_linea_inferior < distancia_stock_limite_superior)){
                    //precio del stock pegado al límite inferior
                    //si la distancia que hay entre la línea media y la línea superior * 2 es menor a la distancia que hay entre el precio actual del stock y la línea media, entonces se trata de un pico al que debemos responder contra tendencia
                    if((Math.abs(punto_linea_media.y-punto_linea_superior.y)*1)<=Math.abs(punto_linea_media.y-punto_stock_actual.y)){
                        System.out.println("Pico encontrado contra-tendencia: subir");
                        presionarBotonSubir(imagen_original);
                        compra_realizada = 1;
                        numero_compras++;
                        return;
                    }
                }
                getRobot().mouseMove((int)punto_limite_compra.x, (int)punto_limite_compra.y);
                /*getRobot().mousePress(InputEvent.BUTTON1_DOWN_MASK);
                getRobot().mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                getRobot().mousePress(InputEvent.BUTTON1_DOWN_MASK);
                getRobot().mouseRelease(InputEvent.BUTTON1_DOWN_MASK);*/
            }else if(distancia_stock_limite_compra <= 40 && distancia_stock_limite_compra >= 10){
                //razonamiento extendido
                /*
                Este razonamiento es cuando se está muy cerca del límite de compra (40 - 10) pixeles, es decir, la ventana de observación es de 30px
                Si el precio del stock está cercano a algún límite, entonces se realiza el siguiente razonamiento:
                Si el precio del stock está dentro de 2x la distancia del límite opuesto con respecto de la línea media, entonces se sigue la tendencia -> "Apenas vamos hacia esa dirección, entonces seguimos la tendencia"
                Si el precio del stock > 2x la distancia del límite opuesto con respecto de la línea media, entonces se va contra tendencia -> "Ya estamos muy arriba de esa tendencia, entonces asumimos que habrá rebote"
                */
                /*if(distancia_stock_limite_superior < distancia_stock_linea_inferior){
                    //pegado al límite superior
                    //si la distancia a la que se encuentra el stock con respecto a la linea media es mayor a 1x y menor o igual a 2x donde x representa la distancia que existe entre la linea media y el límite contrario, entonces se sigue la tendencia
                    if(((Math.abs(punto_linea_verde.y-punto_linea_media.y)*1)<Math.abs(punto_linea_media.y-punto_stock_actual.y))&&((Math.abs(punto_linea_verde.y-punto_linea_media.y)*0.5)>=Math.abs(punto_linea_media.y-punto_stock_actual.y))){
                        System.out.println("sigue-tendencia: subir");
                        presionarBotonSubir(imagen_original);
                        compra_realizada = 1;
                        numero_compras++;
                    }else if((Math.abs(punto_linea_verde.y-punto_linea_media.y)*1.3)<=Math.abs(punto_linea_media.y-punto_stock_actual.y)){
                        System.out.println("contra-tendencia: bajar");
                        presionarBotonBajar(imagen_original);
                        compra_realizada = 1;
                        numero_compras++;
                    }
                }else if(distancia_stock_linea_media > distancia_stock_linea_inferior){
                    //estamos pegamos al límite inferior
                    if(((Math.abs(punto_linea_superior.y-punto_linea_media.y)*1)<=Math.abs(punto_linea_media.y-punto_stock_actual.y))&&((Math.abs(punto_linea_superior.y-punto_linea_media.y)*0.5)>=Math.abs(punto_linea_media.y-punto_stock_actual.y))){
                        System.out.println("sigue-tendencia: bajar");
                        presionarBotonBajar(imagen_original);
                        compra_realizada = 1;
                        numero_compras++;
                    }else if((Math.abs(punto_linea_superior.y-punto_linea_media.y)*1.3)<=Math.abs(punto_linea_media.y-punto_stock_actual.y)){
                        System.out.println("contra-tendencia: Subir");
                        presionarBotonSubir(imagen_original);
                        compra_realizada = 1;
                        numero_compras++;
                    }
                }*/
            }
            /*if(lista_imagenes == null) lista_imagenes = new LinkedList<Mat>();
            Core.rectangle(imagen_original, punto_superior_izquierdo_parte_central, punto_inferior_derecho_parte_central, new Scalar(0,0,255));
            Core.rectangle(imagen_original, getRectagunloBotones().tl(), getRectagunloBotones().br(), new Scalar(0,0,255));
            Core.line(imagen_original, punto_stock_actual, punto_stock_actual, new Scalar(0,255,0), 4);
            Core.line(imagen_original, punto_linea_superior, punto_linea_superior, new Scalar(0,255,0), 4);
            Core.line(imagen_original, punto_linea_verde, punto_linea_verde, new Scalar(0,255,0), 4);
            Core.line(imagen_original, punto_linea_media, punto_linea_media, new Scalar(0,255,0), 4);
            Core.line(imagen_original, punto_limite_compra, punto_limite_compra, new Scalar(0,255,0), 4);

            lista_imagenes.add(imagen_original.clone());
            numero_imagen++;*/
        }else{
            org.opencv.core.Point punto_stock_actual = getPuntoDeStockActual(imagen_original);
            //si alguno de los puntos es null, entonces no se continua con el analisis
            org.opencv.core.Point punto_linea_final = getPuntoDeLineaFinal(imagen_original);
            if((punto_stock_actual != null && punto_linea_final != null)&&(punto_linea_final.x - punto_stock_actual.x)<=30){
                if(lista_imagenes == null) lista_imagenes = new LinkedList<Mat>();
                lista_imagenes.add(imagen_original.clone());
                numero_imagen++;
            }
            
            //como ya se realizó la compra, entonces buscamos la ventana de resultados. Una vez que tengamos la ventana de resultados,
            //la guardamos, presionamos cualquier lado fuera de la ventana y posicionamos el mouse fuera de la ventana para que no cause problema
            //se debería reiniciar el proceso de busqueda de otra oportunidad
            org.opencv.core.Point punto_ventana = getPuntoMasDerechaDeVentanaDeResultados(imagen_original);
            if(punto_ventana == null) return;
            numero_intentos_buscando_ventana_resultados++;
            //System.out.println("Intentos buscando la ventana de resultados: "+numero_intentos_buscando_ventana_resultados);
               
            //ahora que ya se encontró la ventana de resultados, pero hay que asegurarnos de que ya aparecieron los resultados de la transacción, es decir
            //que aparezca una cantidad en blanco, que sería o correspondería a pérdida y de color verde que correspondería a ganancia
            Boolean hay_resultado = true;
            //may 148,184,33
            if(hayColorDentroDeZonaDeResultados(imagen_original.submat(getRectanguloResultado()), new Scalar(127,149,32), new Scalar(148,182,33))){
                System.out.println("Resultado: 1");
                aciertos++;
            }else if(hayColorDentroDeZonaDeResultados(imagen_original.submat(getRectanguloResultado()), new Scalar(255,255,255), new Scalar(255,255,255))){
                System.out.println("Resultado: 0");
            }else hay_resultado = false;

            if(hay_resultado){
                //ya se mostró la ventana, entonces hacemos click fuera de ella para que desaparezca
                getRobot().mouseMove((int)200, (int)200);
                getRobot().mousePress(InputEvent.BUTTON1_DOWN_MASK);
                getRobot().mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                getRobot().mousePress(InputEvent.BUTTON1_DOWN_MASK);
                getRobot().mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                //después de que se quita la ventana de resultados, nos movemos fuera de la gráfica para no interferir con la visualización
                getRobot().mouseMove(0, 0);
                Highgui.imwrite("imagenes/resultado_"+numero_compras+".png", imagen_original);

                //escribimos las imagenes que tenemos en la lista de imagenes, ahorita que es posible
                if(lista_imagenes != null){
                    int i=0;
                    for(Mat image : lista_imagenes){
                        Highgui.imwrite("imagenes/imagen_"+numero_imagen+"_"+i+".png", image);
                        i++;
                    }
                }
                lista_imagenes = null;
                compra_realizada = 0;
                numero_intentos_buscando_ventana_resultados = 0;

                if(numero_compras >= this.limite_numero_compras){
                    System.out.println("Aciertos: "+aciertos+" Errores: "+(limite_numero_compras-aciertos)+" %Aciertos: "+(aciertos*100/limite_numero_compras)+" %Errores: "+(((limite_numero_compras-aciertos)*100)/limite_numero_compras));
                    this.ejecutando = false;
                    return;
                }
            }
        }
    }
    
    public org.opencv.core.Point getPuntoDeLineaFinal(Mat imagen_original){
        Mat linea_final = new Mat();
        //may 59,152,151
        linea_final = getMatConSoloElementosDentroDelRangoDeColor(imagen_original.submat(this.getRectanguloCentral()), new Scalar(59,152,151), new Scalar(59,152,151));
        //muestraImagenEnVentana(convertirMatrizAImagen(ventana_resultados), "Ventana resultados");
        //Highgui.imwrite("imagenes/viendo_resultados_"+numero_imagen+".png", imagen_original);
        //Highgui.imwrite("imagenes/buscando_ventana_resultados_"+numero_imagen+".png", ventana_resultados);
        org.opencv.core.Point punto_mas_ala_derecha = getPuntoMasAlaDerechaDentroDeZona(linea_final);
        if(punto_mas_ala_derecha == null) return null;
        org.opencv.core.Point punto_referencia = this.getRectanguloCentral().tl();
        punto_mas_ala_derecha.x += punto_referencia.x;
        punto_mas_ala_derecha.y += punto_referencia.y;
        //Core.line(imagen_original, punto_mas_ala_derecha, punto_mas_ala_derecha, new Scalar(0,255,0), 4);
        //muestraImagenEnVentana(convertirMatrizAImagen(imagen_original), "Linea verde inferior");
        return punto_mas_ala_derecha;
    }
    
    public Boolean hayColorDentroDeZonaDeResultados(Mat imagen_original, Scalar color_inferior, Scalar color_superior){
        return hayValorBlanco(getMatConSoloElementosDentroDelRangoDeColor(imagen_original, color_inferior, color_superior));
    }
    
    public Boolean hayValorBlanco(Mat imagen){
        org.opencv.core.Point punto_actual = new org.opencv.core.Point(0,0);
        
        double[] valores = null;
        for(int i=0; i<imagen.cols(); i++){
            for(int j=0; j<imagen.rows(); j++){
                valores = imagen.get(j, i);
                if(valores[0] > 0) return true;
            }
        }
        return false;
    }
    
    public org.opencv.core.Point getPuntoMasDerechaDeVentanaDeResultados(Mat imagen_original){
        Mat ventana_resultados = new Mat();
        //may 39,26,21
        ventana_resultados = getMatConSoloElementosDentroDelRangoDeColor(imagen_original.submat(this.getRectanguloVentanaDeResultados()), new Scalar(36,24,21), new Scalar(36,24,21));
        //muestraImagenEnVentana(convertirMatrizAImagen(ventana_resultados), "Ventana resultados");
        //Highgui.imwrite("imagenes/viendo_resultados_"+numero_imagen+".png", imagen_original);
        //Highgui.imwrite("imagenes/buscando_ventana_resultados_"+numero_imagen+".png", ventana_resultados);
        org.opencv.core.Point punto_mas_ala_derecha = getPuntoMasAlaDerechaDentroDeZona(ventana_resultados);
        if(punto_mas_ala_derecha == null) return null;
        org.opencv.core.Point punto_referencia = this.getRectanguloVentanaDeResultados().tl();
        punto_mas_ala_derecha.x += punto_referencia.x;
        punto_mas_ala_derecha.y += punto_referencia.y;
        //Core.line(imagen_original, punto_mas_ala_derecha, punto_mas_ala_derecha, new Scalar(0,255,0), 4);
        //muestraImagenEnVentana(convertirMatrizAImagen(imagen_original), "Linea verde inferior");
        return punto_mas_ala_derecha;
    }
    
    public void presionarBotonSubir(Mat imagen_original){
        Mat boton_subir = new Mat();
        //may 109,191,33
        boton_subir = getMatConSoloElementosDentroDelRangoDeColor(imagen_original.submat(this.getRectagunloBotones()), new Scalar(110,191,33), new Scalar(110,191,33));
        org.opencv.core.Point punto_mas_ala_derecha = getPuntoMasAlaDerechaDentroDeZona(boton_subir);
        if(punto_mas_ala_derecha == null) return;
        org.opencv.core.Point punto_referencia = this.getRectagunloBotones().tl();
        punto_mas_ala_derecha.x += punto_referencia.x;
        punto_mas_ala_derecha.y += punto_referencia.y;
        
        //movemos el mouse a esa direccion, para hacer click
        getRobot().mouseMove((int)punto_mas_ala_derecha.x-10, (int)punto_mas_ala_derecha.y-10);
        getRobot().mousePress(InputEvent.BUTTON1_DOWN_MASK);
        getRobot().mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
        getRobot().mousePress(InputEvent.BUTTON1_DOWN_MASK);
        getRobot().mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
    }
    
    public void presionarBotonBajar(Mat imagen_original){
        Mat boton_bajar = new Mat();
        //may 82,25,175
        boton_bajar = getMatConSoloElementosDentroDelRangoDeColor(imagen_original.submat(this.getRectagunloBotones()), new Scalar(86,25,172), new Scalar(86,25,172));
        org.opencv.core.Point punto_mas_ala_derecha = getPuntoMasAlaDerechaDentroDeZona(boton_bajar);
        if(punto_mas_ala_derecha == null) return;
        org.opencv.core.Point punto_referencia = this.getRectagunloBotones().tl();
        punto_mas_ala_derecha.x += punto_referencia.x;
        punto_mas_ala_derecha.y += punto_referencia.y;
        
        //movemos el mouse a esa direccion, para hacer click
        getRobot().mouseMove((int)punto_mas_ala_derecha.x-10, (int)punto_mas_ala_derecha.y-10);
        getRobot().mousePress(InputEvent.BUTTON1_DOWN_MASK);
        getRobot().mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
        getRobot().mousePress(InputEvent.BUTTON1_DOWN_MASK);
        getRobot().mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
    }
    
    public org.opencv.core.Point getPuntoDeLineaLimiteDeCompra(Mat imagen_original){
        Mat linea_limite_compra = new Mat();
        linea_limite_compra = getMatConSoloElementosDentroDelRangoDeColor(imagen_original.submat(getRectanguloCentral()), new Scalar(100,99,99), new Scalar(100,99,99));
        //muestraImagenEnVentana(convertirMatrizAImagen(linea_limite_compra), "Linea limite compra");
        
        org.opencv.core.Point punto_mas_ala_derecha = getPuntoMasAlaDerechaDentroDeZona(linea_limite_compra);
        if(punto_mas_ala_derecha == null) return null;
        org.opencv.core.Point punto_referencia = this.getRectanguloCentral().tl();
        punto_mas_ala_derecha.x += punto_referencia.x;
        punto_mas_ala_derecha.y += punto_referencia.y;
        //Core.line(imagen_original, punto_mas_ala_derecha, punto_mas_ala_derecha, new Scalar(0,255,0), 4);
        //muestraImagenEnVentana(convertirMatrizAImagen(imagen_original), "Linea verde inferior");
        return punto_mas_ala_derecha;
    }
    
    public double getDistanciaEuclidianaDeDosPuntos(org.opencv.core.Point punto_uno, org.opencv.core.Point punto_dos){
        return Math.sqrt(Math.pow((punto_dos.y-punto_uno.y), 2.0)+Math.pow((punto_dos.x-punto_uno.x),2.0));
    }
    
    public org.opencv.core.Point getUltimoPuntoDeLineaInferior(Mat imagen_original){
        Mat linea_verde_inferior = new Mat();
        linea_verde_inferior = getMatConSoloElementosDentroDelRangoDeColor(imagen_original.submat(getRectanguloCentral()), new Scalar(23,123,71), new Scalar(39,199,110));
        //muestraImagenEnVentana(convertirMatrizAImagen(linea_verde_inferior), "Linea verde inferior");
        
        org.opencv.core.Point punto_mas_ala_derecha = getPuntoMasAlaDerechaDentroDeZona(linea_verde_inferior);
        if(punto_mas_ala_derecha == null) return null;
        org.opencv.core.Point punto_referencia = this.getRectanguloCentral().tl();
        punto_mas_ala_derecha.x += punto_referencia.x;
        punto_mas_ala_derecha.y += punto_referencia.y;
        //Core.line(imagen_original, punto_mas_ala_derecha, punto_mas_ala_derecha, new Scalar(0,255,0), 4);
        //muestraImagenEnVentana(convertirMatrizAImagen(imagen_original), "Linea verde inferior");
        return punto_mas_ala_derecha;
    }
    
    public org.opencv.core.Point getUltimoPuntoDeLineaMedia(Mat imagen_original){
        Mat linea_amarilla_media = new Mat();
        //server may 16,186,236 17,187,237
        linea_amarilla_media = getMatConSoloElementosDentroDelRangoDeColor(imagen_original.submat(getRectanguloCentral()), new Scalar(17,187,237), new Scalar(17,187,237));
        //muestraImagenEnVentana(convertirMatrizAImagen(linea_amarilla_media), "Linea amarilla media");
        
        org.opencv.core.Point punto_mas_ala_derecha = getPuntoMasAlaDerechaDentroDeZona(linea_amarilla_media);
        if(punto_mas_ala_derecha == null) return null;
        org.opencv.core.Point punto_referencia = this.getRectanguloCentral().tl();
        punto_mas_ala_derecha.x += punto_referencia.x;
        punto_mas_ala_derecha.y += punto_referencia.y;
        //Core.line(imagen_original, punto_mas_ala_derecha, punto_mas_ala_derecha, new Scalar(0,255,0), 4);
        //muestraImagenEnVentana(convertirMatrizAImagen(imagen_original), "Linea amarilla media");
        return punto_mas_ala_derecha;
    }
    
    public org.opencv.core.Point getUltimoPuntoDeLineaSuperior(Mat imagen_original){
        Mat linea_roja_superior = new Mat();
        linea_roja_superior = getMatConSoloElementosDentroDelRangoDeColor(imagen_original.submat(getRectanguloCentral()), new Scalar(14,0,127), new Scalar(20,0,226));
        //muestraImagenEnVentana(convertirMatrizAImagen(linea_roja_superior), "Linea roja superior");
        
        org.opencv.core.Point punto_mas_ala_derecha = getPuntoMasAlaDerechaDentroDeZona(linea_roja_superior);
        if(punto_mas_ala_derecha == null) return null;
        org.opencv.core.Point punto_referencia = this.getRectanguloCentral().tl();
        punto_mas_ala_derecha.x += punto_referencia.x;
        punto_mas_ala_derecha.y += punto_referencia.y;
        //Core.line(imagen_original, punto_mas_ala_derecha, punto_mas_ala_derecha, new Scalar(0,255,0), 4);
        //muestraImagenEnVentana(convertirMatrizAImagen(imagen_original), "Linea roja superior");
        
        return punto_mas_ala_derecha;
    }
    
    //esta función devuelve el punto más a la derecha de los elementos de la matriz y que esté dentro del rectángulo central
    public org.opencv.core.Point getPuntoMasAlaDerechaDentroDeZona(Mat imagen){
        //esta función se reescribe para recuperar el punto más a la derecha pero buscando especificamente de la derecha a izquierda y de abajo hacia arriba
        //cuando encuentre el primer punto es el que devolverá, el más a la derecha y más abajo.
        //se asume que la matriz que se recupera es la submatriz que delimita la zona de busqueda
        double[] valores = null;
        for(int i=imagen.cols()-1; i>=0; i--){
            for(int j=imagen.rows()-1; j>=0; j--){
                //System.out.println("i: "+i+", j: "+j+" imagen: "+imagen.size());
                valores = imagen.get(j, i);
                if(valores != null && valores[0] == 255){
                    return new org.opencv.core.Point(i,j);
                }
            }
        }
        return null;
        /*org.opencv.core.Point punto_actual = new org.opencv.core.Point(0,0);
        double[] valores = null;
        for(int i=0; i<imagen.cols(); i++){
            for(int j=0; j<imagen.rows(); j++){
                valores = imagen.get(j, i);
                if((valores[0] == 255) && (getRectanguloCentral().contains(new org.opencv.core.Point(i,j))) && (punto_actual.x < i)){
                    punto_actual.x = i;
                    punto_actual.y = j;
                }
            }
        }
        return punto_actual;*/
    }
    
    //esta función devuelve el punto más a la derecha de los elementos de la matriz y que esté dentro del rectángulo central
    public org.opencv.core.Point getPuntoMasAlaDerechaDentroDelRectanguloDeBotones(Mat imagen){
        org.opencv.core.Point punto_actual = new org.opencv.core.Point(0,0);
        
        double[] valores = null;
        for(int i=0; i<imagen.cols(); i++){
            for(int j=0; j<imagen.rows(); j++){
                valores = imagen.get(j, i);
                if((valores[0] == 255) && (getRectagunloBotones().contains(new org.opencv.core.Point(i,j))) && (punto_actual.x < i)){
                    punto_actual.x = i;
                    punto_actual.y = j;
                }
            }
        }
        return punto_actual;
    }
    
    //esta función devuelve el punto más a la derecha de la zona de la ventana o donde deberá estar la ventana
    public org.opencv.core.Point getPuntoMasAlaDerechaDentroDeVentanaDeResultados(Mat imagen){
        org.opencv.core.Point punto_actual = new org.opencv.core.Point(0,0);
        
        double[] valores = null;
        for(int i=0; i<imagen.cols(); i++){
            for(int j=0; j<imagen.rows(); j++){
                valores = imagen.get(j, i);
                if((valores[0] == 255) && (getRectanguloVentanaDeResultados().contains(new org.opencv.core.Point(i,j))) && (punto_actual.x < i)){
                    punto_actual.x = i;
                    punto_actual.y = j;
                }
            }
        }
        return punto_actual;
    }
    
    public org.opencv.core.Point getPuntoDeStockActual(Mat imagen_original){
        Mat precio_actual_stock_blanco = new Mat();
        Mat precio_actual_stock_azul = new Mat();
        //recuperamos las zonas de interes del circulo blanco del precio del stock
        precio_actual_stock_blanco = getMatConSoloElementosDentroDelRangoDeColor(imagen_original, new Scalar(255,255,255), new Scalar(255,255,255));
        //recuperamos la linea azul del stock para validar que el circulo del stock detectado corresponde con el real y no se trate de otro circulo
        precio_actual_stock_azul = getMatConSoloElementosDentroDelRangoDeColor(imagen_original, new Scalar(188,123,29), new Scalar(188,123,29));
        //calculamos la transformada de hough para encontrar los circulos en la imagen del stock, el cual deberá encontrar el circulo del precio actual del stock
        Mat circulos = new Mat();
        Imgproc.HoughCircles(precio_actual_stock_blanco, circulos, Imgproc.CV_HOUGH_GRADIENT, 1, 1, 100, 6, 3, 6);
        
        org.opencv.core.Point pt = null;
        for (int i = 0; i < circulos.cols(); i++) {
            double[] vCircle = circulos.get(0, i);

            pt = new org.opencv.core.Point((int)Math.round(vCircle[0]), (int)Math.round(vCircle[1]));
            int radius = (int)Math.round(vCircle[2]);
            
            //si el circulo está dentro del rectangulo central
            if(getRectanguloCentral().contains(pt)){
                if(hayColorDentroDeLaMat(255, precio_actual_stock_azul.submat(new Rect(radius, radius, (int)pt.x, (int)pt.y)))){
                    Core.circle(imagen_original, pt, radius, new Scalar(255, 0, 0), 1);
                    //System.out.println("Circulo dibujado: ("+pt.x+","+pt.y+")");
                    break;
                }
            }
        }
        return pt;
    }
    
    public Boolean hayColorDentroDeLaMat(int color, Mat matriz){
        double[] valores = null;
        for(int i=0; i<matriz.cols(); i++){
            for(int j=0; j<matriz.rows(); j++){
                valores = matriz.get(j, i);
                if(valores[0] == color) return true;
            }
        }
        return false;
    }
    
    public Mat getMatConSoloElementosDentroDelRangoDeColor(Mat imagen_original, Scalar color_inferior, Scalar color_superior){
        Mat imagen_filtrada = new Mat();
        Core.inRange(imagen_original, color_inferior, color_superior, imagen_filtrada);
        return imagen_filtrada;
    }
    
    public Mat getBufferedImageToMat(BufferedImage image) {
        Mat mat = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
        BufferedImage nueva_imagen = toBufferedImageOfType(image, BufferedImage.TYPE_3BYTE_BGR);
        byte[] data = ((DataBufferByte) nueva_imagen.getRaster().getDataBuffer()).getData();
        mat.put(0, 0, data);
        return mat;
    }
    
    public static BufferedImage toBufferedImageOfType(BufferedImage original, int type) {
        if (original == null) {
            throw new IllegalArgumentException("original == null");
        }
        if (original.getType() == type) {
            return original;
        }
        BufferedImage image = new BufferedImage(original.getWidth(), original.getHeight(), type);
        Graphics2D g = image.createGraphics();
        try {
            g.setComposite(AlphaComposite.Src);
            g.drawImage(original, 0, 0, null);
        }
        finally {
            g.dispose();
        }
        return image;
    }
    
    public BufferedImage capturarPantalla(String nombre_archivo){
        Rectangle screenRectangle = new Rectangle(getTamanioPantalla());
        BufferedImage image = getRobot().createScreenCapture(screenRectangle);
        
        if(nombre_archivo != null){
            try {
                ImageIO.write(image, ".png", new File(nombre_archivo));
            } catch (Exception e) {
                System.out.println("Error al guardar el archivo");
            }
        }
        
        return image;
    }
    
    public static Image convertirMatrizAImagen(Mat matriz){
        MatOfByte matOfByte = new MatOfByte();
        Highgui.imencode(".jpg", matriz, matOfByte);
 
        byte[] byteArray = matOfByte.toArray();
        BufferedImage bufImage = null;
 
        try {
            InputStream in = new ByteArrayInputStream(byteArray);
            bufImage = ImageIO.read(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (Image)bufImage;
    }
    
    public static void muestraImagenEnVentana(Image imagen, String titulo_imagen){
        JFrame frame = new JFrame(titulo_imagen);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
 
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
 
        //Recuperamos la imagen que debera estar referenciada por el path_imagen
        ImageIcon image = new ImageIcon(imagen);
        //ajustamos la ventana al tamaño de la imagen
        frame.setSize(image.getIconWidth()+10,image.getIconHeight()+35);
        
        // Dibujamos la imagen dentro de un BufferedImage
        JLabel label1 = new JLabel(" ", image, JLabel.CENTER);
        frame.getContentPane().add(label1);
 
        frame.validate();
        frame.setVisible(true);
    }
}
